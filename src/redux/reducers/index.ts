import { combineReducers } from 'redux';
import { History } from 'history';
import { IReduxStore } from '..';
import { connectRouter } from 'connected-react-router';
import TabsReducer from './tabsReducer';
import DataReducer from './dataReducer';
import AuthorsReducer from './authorsOptionsReducer';
import BooksReducer from './booksOptionsReducer';
import StoresReducer from './storesOptionsReducer';


export const Reducers = (history: History) => combineReducers<IReduxStore>({
    router: connectRouter(history),
    tabsConfig: TabsReducer,
    data: DataReducer,
    authorsOptions: AuthorsReducer,
    booksOptions: BooksReducer,
    storesOptions: StoresReducer
})