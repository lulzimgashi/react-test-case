import * as _ from 'lodash';
import { DefaultActionTypes } from '../actions';


const intitalState: any = {

};

export default (state = intitalState, action: any) => {

    switch (action.type) {

        case DefaultActionTypes.API_DATA_SUCCESS:
            return _.merge({}, state, action.payload);

        default:
            return state;
    }
}