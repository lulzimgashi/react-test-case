import * as _ from 'lodash';
import CollectionReducerModel, { CollectionNames } from '../../models/CollectionReducerModel';
import { DefaultActionTypes } from '../actions';
import { BookAction } from '../actions/BooksActions/BookActions';
import { BookActionTypes } from '../actions/BooksActions/BookActionTypes';


const intitalState: CollectionReducerModel = {
    isLoading: false,
    isFetched: false,
    currentPage: 1,
    nextPagePossible: false,
    pagesCount: 1,
    pages: {}
};

export default (state = intitalState, action: BookAction) => {

    switch (action.type) {
        case BookActionTypes.GET_BOOKS:

            state.isLoading = true;
            return { ...state };

        case BookActionTypes.CHANGE_PAGE:
            //right now this case is used just to go in previous page
            state.currentPage = action.pageNumber || 1;

            state.nextPagePossible = state.currentPage < state.pagesCount;

            return { ...state };

        case BookActionTypes.SELECT_BOOK:
            state.currentSelected = action.payload
            return { ...state };

        case DefaultActionTypes.API_DATA_SUCCESS:
            if (action.collectionName !== CollectionNames.BOOKS_COLLECTION_NAME) { return state };


            let meta = action.meta
            state.currentPage = meta.page;
            state.nextPagePossible = ((state.currentPage * 10) + 1) === meta.total_resources;
            state.pagesCount = state.nextPagePossible ? state.currentPage + 1 : state.currentPage;

            state.pages[state.currentPage] = Object.keys(action.payload[action.collectionName]);

            state.isLoading = false;
            state.isFetched = true;


            return { ...state };
        default:
            return state;
    }
}