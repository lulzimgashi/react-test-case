import { TabsAction } from '../actions/TabActions/TabActions';
import { TabsActionTypes } from '../actions/TabActions/TabActionTypes';
import TabsConfig from '../../models/TabsConfig';


const intitalState: TabsConfig = {
    currentTab: 0
};

export default (state = intitalState, action: TabsAction) => {

    switch (action.type) {

        case TabsActionTypes.CHANGE_TABE:
            const chosenTab = (action.payload as number)
            state.currentTab = chosenTab;

            return { ...state };
        default:
            return state;
    }
}