import * as _ from 'lodash';
import CollectionReducerModel, { CollectionNames } from '../../models/CollectionReducerModel';
import { DefaultActionTypes } from '../actions';
import { AuthorAction } from '../actions/AuthorActions/AuthorActions';
import { AuthorActionTypes } from '../actions/AuthorActions/AuthorActionTypes';

const intitalState: CollectionReducerModel = {
    currentSelected: null,
    isLoading: false,
    isFetched: false,
    currentPage: 1,
    nextPagePossible: false,
    pagesCount: 1,
    pages: {}
};

export default (state = intitalState, action: AuthorAction) => {

    switch (action.type) {
        case AuthorActionTypes.GET_AUTHORS:

            state.isLoading = true;
            return { ...state };

        case AuthorActionTypes.CHANGE_PAGE:
            //right now this case is used just to go in previous page
            state.currentPage = action.pageNumber || 1;

            state.nextPagePossible = state.currentPage < state.pagesCount;


            return { ...state };

        case AuthorActionTypes.SELECT_AUTHOR:
            state.currentSelected = action.payload
            return { ...state };

        case DefaultActionTypes.API_DATA_SUCCESS:
            if (action.collectionName !== CollectionNames.AUTHORS_COLLECTION_NAME) { return state };


            let meta = action.meta
            state.currentPage = meta.page;
            state.nextPagePossible = ((state.currentPage * 10) + 1) === meta.total_resources;
            state.pagesCount = state.nextPagePossible ? state.currentPage + 1 : state.currentPage;

            state.pages[state.currentPage] = Object.keys(action.payload[action.collectionName]);

            state.isLoading = false;
            state.isFetched = true;


            return { ...state };

        default:
            return state;
    }
}