import { ofType, Epic, StateObservable } from 'redux-observable';
import { map, switchMap } from 'rxjs/operators';
import { ajax } from 'rxjs/ajax';
import build from 'redux-object';
import normalize from 'json-api-normalizer';
import { IReduxStore } from '..';
import { DefaultActionTypes } from '../actions';
import { API_URL } from 'src/config';
import { BookAction } from '../actions/BooksActions/BookActions';
import { BookActionTypes } from '../actions/BooksActions/BookActionTypes';
import { CollectionNames } from 'src/models/CollectionReducerModel';
import { selectBookAction } from '../actions/BooksActions/BookActionCreators';


const getAllBooksEpic: Epic<BookAction, BookAction> = (action$, state: StateObservable<IReduxStore>) =>
    action$.pipe(
        ofType(BookActionTypes.GET_BOOKS),
        switchMap((action: BookAction) =>
            ajax({
                url: API_URL + 'books?sort=title&include=author&fields[authors]=name&page[number]=' + action.pageNumber,
                headers: {
                    'Content-Type': "application/vnd.api+json"
                }
            }).pipe(
                map((ajaxResponse: any) => {
                    return {
                        type: DefaultActionTypes.API_DATA_SUCCESS,
                        payload: Object.assign({}, normalize(ajaxResponse.response)),
                        meta: ajaxResponse.response.meta,
                        collectionName: action.collectionName
                    }
                })
            )
        )

    );


const getSingleBookEpic: Epic<BookAction, BookAction> = (action$, state: StateObservable<IReduxStore>) =>
    action$.pipe(
        ofType(BookActionTypes.GET_SINGLE_BOOK),
        switchMap((action: BookAction) =>
            ajax({
                url: API_URL + `books/${action.payload}?include=photos,author`,
                headers: {
                    'Content-Type': "application/vnd.api+json"
                }
            }).pipe(
                map((ajaxResponse: any) => {
                    const book = build(normalize(ajaxResponse.response), CollectionNames.BOOKS_COLLECTION_NAME, action.payload);
                    book.photoLoaded = true;

                    return selectBookAction(book);
                })
            )
        )

    );



const getBookPhotosEpic: Epic<BookAction, BookAction> = (action$, state: StateObservable<IReduxStore>) =>
    action$.pipe(
        ofType(BookActionTypes.GET_SINGLE_BOOK_PHOTOS),
        switchMap((action: BookAction) =>
            ajax({
                url: API_URL + `books/${action.payload}/photos`,
                headers: {
                    'Content-Type': "application/vnd.api+json"
                }
            }).pipe(
                map((ajaxResponse: any) => {
                    //normalize data
                    const normalizedData = normalize(ajaxResponse.response);
                    //transform normalized data to array of photos
                    const photos = build(normalizedData, CollectionNames.PHOTOS_COLLECTION_NAME, null);

                    action.callback && action.callback(photos);
                    return { type: 'none' };
                }),
            )
        )

    );



export const BookEpics = [getAllBooksEpic, getSingleBookEpic, getBookPhotosEpic];