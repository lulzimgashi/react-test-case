import { ofType, Epic, StateObservable } from 'redux-observable';
import build from 'redux-object';
import { map, switchMap } from 'rxjs/operators';
import { ajax } from 'rxjs/ajax';
import normalize from 'json-api-normalizer';
import { IReduxStore } from '..';
import { DefaultActionTypes } from '../actions';
import { AuthorAction } from '../actions/AuthorActions/AuthorActions';
import { AuthorActionTypes } from '../actions/AuthorActions/AuthorActionTypes';
import { API_URL } from 'src/config';
import { selectAuthorAction } from '../actions/AuthorActions/AuthorActionCreators';
import { CollectionNames } from 'src/models/CollectionReducerModel';


const getAllAuthorsEpic: Epic<AuthorAction, AuthorAction> = (action$, state: StateObservable<IReduxStore>) =>
    action$.pipe(
        ofType(AuthorActionTypes.GET_AUTHORS),
        switchMap((action: AuthorAction) =>
            ajax({
                url: API_URL + 'authors?sort=name&include=books&page[number]=' + action.pageNumber,
                headers: {
                    'Content-Type': "application/vnd.api+json"
                }
            }).pipe(
                map((ajaxResponse: any) => {
                    return {
                        type: DefaultActionTypes.API_DATA_SUCCESS,
                        payload: Object.assign({}, normalize(ajaxResponse.response)),
                        meta: ajaxResponse.response.meta,
                        collectionName: action.collectionName
                    }
                })
            )
        )

    );


const getSingleAuthorEpic: Epic<AuthorAction, AuthorAction> = (action$, state: StateObservable<IReduxStore>) =>
    action$.pipe(
        ofType(AuthorActionTypes.GET_SINGLE_AUTHOR),
        switchMap((action: AuthorAction) =>
            ajax({
                url: API_URL + `authors/${action.payload}?include=photos,books`,
                headers: {
                    'Content-Type': "application/vnd.api+json"
                }
            }).pipe(
                map((ajaxResponse: any) => {
                    const author = build(normalize(ajaxResponse.response), CollectionNames.AUTHORS_COLLECTION_NAME, action.payload);
                    author.photoLoaded = true;

                    return selectAuthorAction(author);
                })
            )
        )

    );


const getAuthorPhotosEpic: Epic<AuthorAction, AuthorAction> = (action$, state: StateObservable<IReduxStore>) =>
    action$.pipe(
        ofType(AuthorActionTypes.GET_SINGLE_AUTHOR_PHOTOS),
        switchMap((action: AuthorAction) =>
            ajax({
                url: API_URL + `authors/${action.payload}/photos`,
                headers: {
                    'Content-Type': "application/vnd.api+json"
                }
            }).pipe(
                map((ajaxResponse: any) => {
                    //normalize data
                    const normalizedData = normalize(ajaxResponse.response);
                    //transform normalized data to array of photos
                    const photos = build(normalizedData, CollectionNames.PHOTOS_COLLECTION_NAME, null);

                    action.callback && action.callback(photos);
                    return { type: 'none' };
                }),
            )
        )

    );

export const AuthorEpics = [getAllAuthorsEpic, getSingleAuthorEpic, getAuthorPhotosEpic];