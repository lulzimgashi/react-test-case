import { ofType, Epic, StateObservable } from 'redux-observable';
import { map, switchMap } from 'rxjs/operators';
import { ajax } from 'rxjs/ajax';
import normalize from 'json-api-normalizer';
import { IReduxStore } from '..';
import { DefaultActionTypes } from '../actions';
import { API_URL } from 'src/config';
import { StoreAction } from '../actions/StoresActions/StoreActions';
import { StoreActionTypes } from '../actions/StoresActions/StoreActionTypes';



const getAllStoresEpic: Epic<StoreAction, StoreAction> = (action$, state: StateObservable<IReduxStore>) =>
    action$.pipe(
        ofType(StoreActionTypes.GET_STORES),
        switchMap((action: StoreAction) =>
            ajax({
                url: API_URL + 'stores?page[number]=' + action.pageNumber,
                headers: {
                    'Content-Type': "application/vnd.api+json"
                }
            }).pipe(
                map((ajaxResponse: any) => {
                    return {
                        type: DefaultActionTypes.API_DATA_SUCCESS,
                        payload: Object.assign({}, normalize(ajaxResponse.response)),
                        meta: ajaxResponse.response.meta,
                        collectionName: action.collectionName
                    }
                })
            )
        )

    );


export const StoreEpics = [getAllStoresEpic];