import { combineEpics } from 'redux-observable';
import { AuthorEpics } from './authorEpics';
import { BookEpics } from './bookEpics';
import { StoreEpics } from './storeEpics';


export const Epics = combineEpics(...AuthorEpics, ...BookEpics, ...StoreEpics);