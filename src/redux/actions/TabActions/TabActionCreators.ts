import { TabsActionTypes } from './TabActionTypes'
import { TabsAction } from './TabActions'

export const changeTabAction = (tab: number) => {
    const ChangeTabAction: TabsAction = {
        type: TabsActionTypes.CHANGE_TABE,
        payload: tab
    }

    return ChangeTabAction;
}

