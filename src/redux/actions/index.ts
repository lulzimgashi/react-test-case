export interface DefaultAction {
    collectionName?: string,
    meta?: any
}

export enum DefaultActionTypes {
    API_DATA_SUCCESS = "@@default_action/api_data_success"
}
