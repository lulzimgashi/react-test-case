import { AuthorActionTypes } from './AuthorActionTypes'
import { AuthorAction } from './AuthorActions'
import { DefaultAction } from '..'
import { CollectionNames } from 'src/models/CollectionReducerModel'

const defaultAuthorAction: DefaultAction = { collectionName: CollectionNames.AUTHORS_COLLECTION_NAME }

export const getAuthorsAction = (pageNumber?: number) => {
    const GetAuthorsAction: AuthorAction = {
        ...defaultAuthorAction,
        type: AuthorActionTypes.GET_AUTHORS,
        pageNumber: pageNumber || 1
    }

    return GetAuthorsAction;
}


export const changePageAuthorAction = (pageNumber: number) => {
    const ChangePageNumberAction: AuthorAction = {
        ...defaultAuthorAction,
        type: AuthorActionTypes.CHANGE_PAGE,
        pageNumber
    }

    return ChangePageNumberAction;
}



export const selectAuthorAction = (author: any) => {
    const SelectAuthorAction: AuthorAction = {
        ...defaultAuthorAction,
        type: AuthorActionTypes.SELECT_AUTHOR,
        payload: author
    }

    return SelectAuthorAction;
}


export const getSingleAuthorAction = (authorId: string) => {
    const GetSingleAuthorAction: AuthorAction = {
        ...defaultAuthorAction,
        type: AuthorActionTypes.GET_SINGLE_AUTHOR,
        payload: authorId
    }

    return GetSingleAuthorAction;
}


export const getAuthorPhotosAction = (authorId: string, callback?: (data: any) => void) => {
    const GetAuthorPhotosAction: AuthorAction = {
        ...defaultAuthorAction,
        type: AuthorActionTypes.GET_SINGLE_AUTHOR_PHOTOS,
        payload: authorId,
        callback
    }

    return GetAuthorPhotosAction;
}


