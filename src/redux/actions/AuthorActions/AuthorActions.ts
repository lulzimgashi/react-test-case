import { Action } from 'redux';
import { DefaultAction } from '..';

export interface AuthorAction extends Action, DefaultAction {
    payload?: any,
    pageNumber?: number,
    callback?: (data: any) => void
}
