export enum AuthorActionTypes {
    GET_AUTHORS = "@@author/get_authors",
    GET_SINGLE_AUTHOR = "@@author/get_single_author",
    GET_SINGLE_AUTHOR_PHOTOS = "@@author/get_single_author_photos",
    SELECT_AUTHOR = "@@author/select_author",
    CHANGE_PAGE = "@@author/change_page"
}
