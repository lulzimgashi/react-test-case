import { Action } from 'redux';
import { DefaultAction } from '..';

export interface StoreAction extends Action, DefaultAction {
    payload?: any,
    pageNumber?: number
}
