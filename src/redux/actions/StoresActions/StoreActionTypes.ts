export enum StoreActionTypes {
    GET_STORES = "@@store/get_stores",
    CHANGE_PAGE = "@@store/change_page"
}
