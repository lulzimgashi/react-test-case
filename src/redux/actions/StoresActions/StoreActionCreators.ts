import { StoreActionTypes } from './StoreActionTypes'
import { StoreAction } from './StoreActions'
import { DefaultAction } from '..'
import { CollectionNames } from 'src/models/CollectionReducerModel'

const defaultStoreAction: DefaultAction = { collectionName: CollectionNames.STORES_COLLECTION_NAME }

export const getStoresAction = (pageNumber?: number) => {
    const GetStoresAction: StoreAction = {
        ...defaultStoreAction,
        type: StoreActionTypes.GET_STORES,
        pageNumber: pageNumber || 1
    }

    return GetStoresAction;
}


export const changePageStoreAction = (pageNumber: number) => {
    const ChangePageNumberAction: StoreAction = {
        ...defaultStoreAction,
        type: StoreActionTypes.CHANGE_PAGE,
        pageNumber
    }

    return ChangePageNumberAction;
}
