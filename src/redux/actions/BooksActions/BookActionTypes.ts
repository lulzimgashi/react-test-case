export enum BookActionTypes {
    GET_BOOKS = "@@book/get_books",
    GET_SINGLE_BOOK = "@@book/get_single_books",
    GET_SINGLE_BOOK_PHOTOS = "@@book/get_single_books_photos",
    SELECT_BOOK = "@@book/select_book",
    CHANGE_PAGE = "@@book/change_page"
}
