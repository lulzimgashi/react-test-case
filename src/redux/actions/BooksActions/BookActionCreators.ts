import { BookActionTypes } from './BookActionTypes'
import { BookAction } from './BookActions'
import { DefaultAction } from '..'
import { CollectionNames } from 'src/models/CollectionReducerModel'

const defaultBookAction: DefaultAction = { collectionName: CollectionNames.BOOKS_COLLECTION_NAME }

export const getBooksAction = (pageNumber?: number) => {
    const GetBooksAction: BookAction = {
        ...defaultBookAction,
        type: BookActionTypes.GET_BOOKS,
        pageNumber: pageNumber || 1
    }

    return GetBooksAction;
}


export const changePageBookAction = (pageNumber: number) => {
    const ChangePageNumberAction: BookAction = {
        ...defaultBookAction,
        type: BookActionTypes.CHANGE_PAGE,
        pageNumber
    }

    return ChangePageNumberAction;
}


export const selectBookAction = (book: any) => {
    const SelectBookAction: BookAction = {
        ...defaultBookAction,
        type: BookActionTypes.SELECT_BOOK,
        payload: book
    }

    return SelectBookAction;
}

export const getSingleBookAction = (book: string) => {
    const GetSingleBookAction: BookAction = {
        ...defaultBookAction,
        type: BookActionTypes.GET_SINGLE_BOOK,
        payload: book
    }

    return GetSingleBookAction;
}


export const getBookPhotosAction = (bookId: string, callback?: (data: any) => void) => {
    const GetBookPhotosAction: BookAction = {
        ...defaultBookAction,
        type: BookActionTypes.GET_SINGLE_BOOK_PHOTOS,
        payload: bookId,
        callback
    }

    return GetBookPhotosAction;
}