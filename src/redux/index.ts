import { applyMiddleware, createStore } from 'redux';
import { createEpicMiddleware } from 'redux-observable';
import { RouterState, routerMiddleware } from 'connected-react-router';
import { createBrowserHistory } from 'history';
import { Epics } from './epics';
import { Reducers } from './reducers';
import TabsConfig from 'src/models/TabsConfig';
import CollectionReducerModel from 'src/models/CollectionReducerModel';

export interface IReduxStore {
    router: RouterState,
    tabsConfig: TabsConfig,
    data: any,
    authorsOptions: CollectionReducerModel,
    booksOptions: CollectionReducerModel,
    storesOptions: CollectionReducerModel
}

const epicMiddleware = createEpicMiddleware();
export const history = createBrowserHistory();


export const store = createStore<IReduxStore, any, any, any>(Reducers(history), applyMiddleware(epicMiddleware, routerMiddleware(history)))


epicMiddleware.run(Epics);