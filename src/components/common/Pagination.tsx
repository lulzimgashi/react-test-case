// @flow
import * as React from 'react';


interface IPaginationProps {
    nextPagePossible: boolean,
    currentPage: number,
    changePage: (pageNumber: number) => void
};



export default function (props: IPaginationProps) {


    const goToBackPage = () => {
        if (props.currentPage === 1) { return };
        props.changePage(props.currentPage - 1);
    }

    const goToNextPage = () => {
        if (!props.nextPagePossible) { return };
        props.changePage(props.currentPage + 1);
    }


    return (
        <nav className="pagination is-small" role="navigation" aria-label="pagination">

            <a onClick={goToBackPage} {...{ disabled: props.currentPage === 1 }} className="pagination-previous">Previous</a>
            <a onClick={goToNextPage} {...{ disabled: !props.nextPagePossible }} className="pagination-next">Next page</a>


            Page : <ul className="pagination-list">
                <li>
                    <a className="pagination-link is-current" aria-label="Page 1" aria-current="page">{props.currentPage}</a>
                </li>
            </ul>
        </nav>
    );
};