// @flow
import * as React from 'react';


interface ILoaderProps {
};



export default function (props: ILoaderProps) {


    return (
        <div className="lds-ring"><div></div><div></div><div></div><div></div></div>
    );
};