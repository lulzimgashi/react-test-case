// @flow
import * as React from 'react';
import TabHeader from './TabHeader';
import TabContent from './TabContent';


interface ITabsProps {
};



export default function (props: ITabsProps) {


    return (
        <div className="container">
            <TabHeader />
            <TabContent />
        </div>
    );
};