// @flow
import * as React from 'react';
import { selectBookAction } from 'src/redux/actions/BooksActions/BookActionCreators';
import { push } from 'connected-react-router';


interface IBookRowItemProps {
    book: any,
    dispatch: (action: any) => void
};



export default function (props: IBookRowItemProps) {


    const goToBookDetails = () => {
        props.dispatch(selectBookAction(props.book));
        props.dispatch(push('/books/' + props.book.id));
    }

    return (
        <tr>
            <td>{props.book.id}</td>
            <td><a onClick={goToBookDetails}>{props.book.title}</a></td>
            <td>{props.book.datePublished}</td>
            <td><a>{props.book.author.name}</a></td>
            <td>{props.book.chapters.length}</td>
        </tr>
    );
};