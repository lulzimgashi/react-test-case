// @flow
import * as React from 'react';
import build from 'redux-object';
import { connect } from 'react-redux';
import { IReduxStore } from 'src/redux';
import CollectionReducerModel, { CollectionNames } from 'src/models/CollectionReducerModel';
import Pagination from '../../../common/Pagination';
import BookRowItem from './BookRowItem';
import { getBooksAction, changePageBookAction } from 'src/redux/actions/BooksActions/BookActionCreators';
import Loader from 'src/components/common/Loader';

interface IBooksTabProps {
    data: any,
    booksOptions: CollectionReducerModel,
    dispatch: (action: any) => void
};



function BooksTab(props: IBooksTabProps) {

    React.useEffect(() => {
        if (!props.booksOptions.isLoading && !props.booksOptions.isFetched) {
            props.dispatch(getBooksAction());
        }
    }, []);


    const changePage = (pageNumber: number) => {
        if (props.booksOptions.currentPage == pageNumber) {
            return;
        }


        if (pageNumber in props.booksOptions.pages) {
            props.dispatch(changePageBookAction(pageNumber));
        } else {
            props.dispatch(getBooksAction(pageNumber));
        }
    }



    const renderRows = () => {
        if (!props.booksOptions.isFetched) { return [] };

        const books = build(props.data, CollectionNames.BOOKS_COLLECTION_NAME, props.booksOptions.pages[props.booksOptions.currentPage]);


        return books.map((book: any) => {
            return <BookRowItem key={book.id} book={book} dispatch={props.dispatch} />
        })
    }


    return (
        <div className='container'>

            <table className="table is-fullwidth is-hoverable">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Title</th>
                        <th>Date Published</th>
                        <th>Author</th>
                        <th>Chapters</th>
                    </tr>
                </thead>

                <tfoot>
                    <tr>
                        <td colSpan={5}>
                            <Pagination
                                currentPage={props.booksOptions.currentPage}
                                nextPagePossible={props.booksOptions.nextPagePossible}
                                changePage={changePage} />
                        </td>
                    </tr>
                </tfoot>

                <tbody>
                    {props.booksOptions.isLoading ?
                        <tr><td><div className='loader-wrapper'><Loader /> Loading Data</div></td></tr> :
                        renderRows()}
                </tbody>


            </table>

        </div>
    );
};


const mapStateToProps = (state: IReduxStore) => {

    return {
        booksOptions: state.booksOptions,
        data: state.data
    }
}

export default connect(mapStateToProps)(BooksTab);