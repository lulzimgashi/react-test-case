// @flow
import * as React from 'react';


interface IStoreRowItemProps {
    store: any
};



export default function (props: IStoreRowItemProps) {


    return (
        <tr>
            <td>{props.store.id}</td>
            <td><a>{props.store.name}</a></td>
            <td>{props.store.address}</td>
            <td>{props.store.countries.id}</td>
            <td>{props.store.books.length}</td>
        </tr>
    );
};