// @flow
import * as React from 'react';
import build from 'redux-object';
import { connect } from 'react-redux';
import { IReduxStore } from 'src/redux';
import CollectionReducerModel, { CollectionNames } from 'src/models/CollectionReducerModel';
import Pagination from '../../../common/Pagination';
import StoreRowItem from './StoreRowItem';
import { getStoresAction, changePageStoreAction } from 'src/redux/actions/StoresActions/StoreActionCreators';
import Loader from 'src/components/common/Loader';

interface IStoresTabProps {
    data: any,
    storesOptions: CollectionReducerModel,
    dispatch: (action: any) => void
};



function StoresTab(props: IStoresTabProps) {

    React.useEffect(() => {
        if (!props.storesOptions.isLoading && !props.storesOptions.isFetched) {
            props.dispatch(getStoresAction());
        }
    }, []);


    const changePage = (pageNumber: number) => {
        if (props.storesOptions.currentPage == pageNumber) {
            return;
        }


        if (pageNumber in props.storesOptions.pages) {
            props.dispatch(changePageStoreAction(pageNumber));
        } else {
            props.dispatch(getStoresAction(pageNumber));
        }
    }



    const renderRows = () => {
        if (!props.storesOptions.isFetched) { return [] };

        const stores = build(props.data, CollectionNames.STORES_COLLECTION_NAME, props.storesOptions.pages[props.storesOptions.currentPage]);


        return stores.map((store: any) => {
            return <StoreRowItem key={store.id} store={store} />
        })
    }


    return (
        <div className='container'>

            <table className="table is-fullwidth is-hoverable">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Address</th>
                        <th>Country</th>
                        <th>Books</th>
                    </tr>
                </thead>

                <tfoot>
                    <tr>
                        <td colSpan={5}>
                            <Pagination
                                currentPage={props.storesOptions.currentPage}
                                nextPagePossible={props.storesOptions.nextPagePossible}
                                changePage={changePage} />
                        </td>
                    </tr>
                </tfoot>

                <tbody>
                    {props.storesOptions.isLoading ?
                        <tr><td><div className='loader-wrapper'><Loader /> Loading Data</div></td></tr> :
                        renderRows()}
                </tbody>


            </table>

        </div>
    );
};


const mapStateToProps = (state: IReduxStore) => {

    return {
        storesOptions: state.storesOptions,
        data: state.data
    }
}

export default connect(mapStateToProps)(StoresTab);