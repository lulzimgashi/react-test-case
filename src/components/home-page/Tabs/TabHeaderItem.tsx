// @flow
import * as React from 'react';


interface ITabHeaderItemProps {
    active: boolean,
    text: string,
    changeTab: (tab: number) => void,
    tab: number
};



export default function (props: ITabHeaderItemProps) {

    const changeTab = () => {
        props.changeTab(props.tab);
    }

    return (
        <li onClick={changeTab} className={props.active ? "is-active" : ''}><a>{props.text}</a></li>
    );
};