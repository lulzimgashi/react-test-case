// @flow
import * as React from 'react';
import { connect } from 'react-redux';
import { IReduxStore } from 'src/redux';
import TabsConfig from 'src/models/TabsConfig';
import AuthorsTab from './AuthorsTab/AuthorsTab';
import BooksTab from './BooksTab/BooksTab';
import StoresTab from './StoresTab/StoresTab';


interface ITabContentProps {
    tabsConfig: TabsConfig
};



function TabContent(props: ITabContentProps) {


    switch (props.tabsConfig.currentTab) {
        case 0:
            return <AuthorsTab />
        case 1:
            return <BooksTab />
        case 2:
            return <StoresTab />
        default:
            return <div />
    }

};


const mapStateToProps = (state: IReduxStore) => {
    return {
        tabsConfig: state.tabsConfig
    }
}

export default connect(mapStateToProps)(TabContent);