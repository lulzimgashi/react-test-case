// @flow
import * as React from 'react';
import { connect } from 'react-redux';
import { IReduxStore } from 'src/redux';
import TabsConfig from 'src/models/TabsConfig';
import TabHeaderItem from './TabHeaderItem';
import { changeTabAction } from 'src/redux/actions/TabActions/TabActionCreators';
import { useEffect } from 'react';


interface ITabHeaderProps {
    tabsConfig: TabsConfig,
    dispatch: (action: any) => void
};



function TabHeader(props: ITabHeaderProps) {

    useEffect(() => {

    }, [props.tabsConfig.currentTab])

    const changeTab = (tab: number) => {
        if (props.tabsConfig.currentTab !== tab) {
            props.dispatch(changeTabAction(tab));
        }
    }

    return (
        <div className="tabs">
            <ul>
                <TabHeaderItem active={props.tabsConfig.currentTab === 0} changeTab={changeTab} tab={0} text='Authors' />
                <TabHeaderItem active={props.tabsConfig.currentTab === 1} changeTab={changeTab} tab={1} text='Books' />
                <TabHeaderItem active={props.tabsConfig.currentTab === 2} changeTab={changeTab} tab={2} text='Stores' />
            </ul>
        </div>
    );
};


const mapStateToProps = (state: IReduxStore) => {
    return {
        tabsConfig: state.tabsConfig
    }
}

export default connect(mapStateToProps)(TabHeader);