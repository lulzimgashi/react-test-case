// @flow
import * as React from 'react';
import build from 'redux-object';
import { connect } from 'react-redux';
import { IReduxStore } from 'src/redux';
import CollectionReducerModel, { CollectionNames } from 'src/models/CollectionReducerModel';
import Pagination from '../../../common/Pagination';
import { getAuthorsAction, changePageAuthorAction } from 'src/redux/actions/AuthorActions/AuthorActionCreators';
import AuthorRowItem from './AuthorRowItem';
import Loader from 'src/components/common/Loader';

interface IAuthorsTabProps {
    data: any,
    authorsOptions: CollectionReducerModel,
    dispatch: (action: any) => void
};



function AuthorsTab(props: IAuthorsTabProps) {

    React.useEffect(() => {
        if (!props.authorsOptions.isLoading && !props.authorsOptions.isFetched) {
            props.dispatch(getAuthorsAction());
        }
    }, []);


    const changePage = (pageNumber: number) => {
        if (props.authorsOptions.currentPage == pageNumber) {
            return;
        }


        if (pageNumber in props.authorsOptions.pages) {
            props.dispatch(changePageAuthorAction(pageNumber));
        } else {
            props.dispatch(getAuthorsAction(pageNumber));
        }
    }



    const renderRows = () => {
        if (!props.authorsOptions.isFetched) { return [] };

        const authors = build(props.data, CollectionNames.AUTHORS_COLLECTION_NAME, props.authorsOptions.pages[props.authorsOptions.currentPage]);

        return authors.map((author: any) => {
            return <AuthorRowItem key={author.id} author={author} dispatch={props.dispatch} />
        })
    }


    return (
        <div className='container'>

            <table className="table is-fullwidth is-hoverable">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Date of birth</th>
                        <th>Date of death</th>
                        <th>Books</th>
                    </tr>
                </thead>

                <tfoot>
                    <tr>
                        <td colSpan={5}>
                            <Pagination
                                currentPage={props.authorsOptions.currentPage}
                                nextPagePossible={props.authorsOptions.nextPagePossible}
                                changePage={changePage} />
                        </td>
                    </tr>
                </tfoot>

                <tbody>
                    {props.authorsOptions.isLoading ?
                        <tr><td><div className='loader-wrapper'><Loader /> Loading Data</div></td></tr> :
                        renderRows()}
                </tbody>


            </table>

        </div>
    );
};


const mapStateToProps = (state: IReduxStore) => {

    return {
        authorsOptions: state.authorsOptions,
        data: state.data
    }
}

export default connect(mapStateToProps)(AuthorsTab);