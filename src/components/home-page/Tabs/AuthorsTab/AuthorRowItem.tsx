// @flow
import * as React from 'react';
import { push } from 'connected-react-router';
import { selectAuthorAction } from 'src/redux/actions/AuthorActions/AuthorActionCreators';


interface IAuthorRowItemProps {
    author: any,
    dispatch: (action: any) => void
};



export default function (props: IAuthorRowItemProps) {

    const renderBooks = () => {

        const books = (props.author.books || []);

        if (!books.length) {
            return <span>-</span>;
        }

        return books.map((book: any, i: any) => {
            return (
                <span key={book.id}><a>{book.title}</a> {(i + 1) < books.length ? '&' : ''} </span>
            )
        });


    }


    const goToAuthor = () => {
        props.dispatch(selectAuthorAction(props.author));
        props.dispatch(push('/authors/' + props.author.id));
    }

    return (
        <tr>
            <td>{props.author.id}</td>
            <td><a onClick={goToAuthor}>{props.author.name}</a></td>
            <td>{props.author.dateOfBirth}</td>
            <td>{props.author.dateOfDeath}</td>
            <td>{renderBooks()}</td>
        </tr>
    );
};