// @flow
import * as React from 'react';
import { connect } from 'react-redux';
import { IReduxStore } from 'src/redux';
import { push } from 'connected-react-router';


interface IBookTableRowProps {
    book: any,
    dispatch: (action: any) => void
};



function BookTableRow(props: IBookTableRowProps) {

    const goToBookDetails = () => {
        props.dispatch(push('/books/' + props.book.id));
    }

    return (
        <tr>
            <td>{props.book.id}</td>
            <td><a onClick={goToBookDetails}>{props.book.title}</a></td>
            <td>{props.book.datePublished}</td>
            <td>{props.book.isbn}</td>
        </tr>
    );
};


const mapStateToProps = (state: IReduxStore) => {
    return {

    }
}


export default connect(mapStateToProps)(BookTableRow);