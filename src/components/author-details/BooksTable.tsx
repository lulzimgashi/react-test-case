// @flow
import * as React from 'react';
import BookTableRow from './BookTableRow';


interface IBooksTableProps {
    author: any
};



export default function (props: IBooksTableProps) {

    const renderRows = () => {
        const books = props.author.books || [];

        return books.map((book: any) => {
            return (
                <BookTableRow key={book.id} book={book} />
            )
        })
    }

    return (
        <table className="table is-fullwidth is-hoverable">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Title</th>
                    <th>Date Published</th>
                    <th>ISBN</th>
                </tr>
            </thead>

            <tbody>
                {renderRows()}
            </tbody>


        </table>
    );
};