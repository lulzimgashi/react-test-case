// @flow
import * as React from 'react';
import { useState, useEffect } from 'react';
import { getAuthorPhotosAction } from 'src/redux/actions/AuthorActions/AuthorActionCreators';
import Loader from '../common/Loader';


interface IPhotoListProps {
    author: any,
    dispatch: (action: any) => void
};



export default function (props: IPhotoListProps) {

    const [loading, setLoading] = useState(false);
    const [photos, setPhotos] = useState(props.author.photos);

    useEffect(() => {

        if (!props.author.photoLoaded && props.author.photos.length) {
            setLoading(true);
            props.dispatch(getAuthorPhotosAction(props.author.id, onPhotoFetched))
        }

    }, []);


    const onPhotoFetched = (data: any) => {
        props.author.photoLoaded = true;
        setPhotos(data || []);
        setLoading(false);

    }

    const renderPhotos = () => {
        return photos.map((photo: any, i: any) => {
            return (
                <figure key={photo.id} style={{ marginRight: '15px' }} className="image is-128x128">
                    <img src={photo.uri} />
                </figure>
            )
        })
    }


    if (loading) {
        return <div className='loader-wrapper'><Loader /> Loading Author Photos</div>
    }

    return (
        <div className='is-flex'>

            {props.author.photoLoaded && renderPhotos()}

        </div>
    );
};