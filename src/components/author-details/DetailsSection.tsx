// @flow
import * as React from 'react';


interface IDetailsSectionProps {
    currentAuthor: any
};



export default function (props: IDetailsSectionProps) {


    return (
        <ul>
            <li>Id : <strong>{props.currentAuthor.id}</strong></li>
            <li>Date of birth : <strong>{props.currentAuthor.dateOfBirth}</strong></li>
            <li>Date of death : <strong>{props.currentAuthor.dateOfDeath}</strong></li>
            <li>Total books : <strong>{props.currentAuthor.books.length}</strong></li>
            <li>Total photos : <strong>{props.currentAuthor.photos.length}</strong></li>
        </ul>
    );
};