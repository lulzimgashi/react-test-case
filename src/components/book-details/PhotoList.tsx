// @flow
import * as React from 'react';
import { useState, useEffect } from 'react';
import Loader from '../common/Loader';
import { getBookPhotosAction } from 'src/redux/actions/BooksActions/BookActionCreators';


interface IPhotoListProps {
    book: any,
    dispatch: (action: any) => void
};



export default function (props: IPhotoListProps) {

    const [loading, setLoading] = useState(false);
    const [photos, setPhotos] = useState(props.book.photos);

    useEffect(() => {

        if (!props.book.photoLoaded && props.book.photos.length) {
            setLoading(true);
            props.dispatch(getBookPhotosAction(props.book.id, onPhotoFetched))
        }

    }, []);


    const onPhotoFetched = (data: any) => {
        props.book.photoLoaded = true;
        setPhotos(data || []);
        setLoading(false);

    }

    const renderPhotos = () => {
        return photos.map((photo: any, i: any) => {
            return (
                <figure key={photo.id} style={{ marginRight: '15px' }} className="image is-128x128">
                    <img src={photo.uri} />
                </figure>
            )
        })
    }


    if (loading) {
        return <div className='loader-wrapper'><Loader /> Loading Book Photos</div>
    }

    return (
        <div className='is-flex'>

            {props.book.photoLoaded && renderPhotos()}

        </div>
    );
};