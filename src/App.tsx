import * as React from 'react';
import Routes from './screens';
import { Switch } from 'react-router';


class App extends React.Component {
  public render() {

    const auth = false;

    //if logged return private routes
    //if public return public routes

    return (
      <Switch>
        <Routes.PublicRoute auth={auth} exact path='/' component={Routes._PublicRoutes.HomePage} />
        <Routes.PublicRoute auth={auth} exact path='/authors/:authorId' component={Routes._PublicRoutes.AuthorDetails} />
        <Routes.PublicRoute auth={auth} exact path='/books/:bookId' component={Routes._PublicRoutes.BookDetails} />

        <Routes.DefaultRoute auth={auth} />
      </Switch>
    );
  }
}

export default App;
