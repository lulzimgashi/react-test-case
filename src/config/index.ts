export const DEFAULT_ROUTES_SETTINGS = {
    default_private_route: "/dashboard",
    default_public_route: "/"
}

export const API_URL = "https://jsonapiplayground.reyesoft.com/v2/"