import * as React from 'react';
import { Redirect } from 'react-router';
import { DEFAULT_ROUTES_SETTINGS } from '..';


export interface IDefaultRouteProps {
    auth: boolean
}

export default class DefaultRoute extends React.Component<IDefaultRouteProps, any> {

    public render() {



        const { auth } = this.props;


        if (auth) {
            return <Redirect to={DEFAULT_ROUTES_SETTINGS.default_private_route} />
        } else {
            return <Redirect to={DEFAULT_ROUTES_SETTINGS.default_public_route} />
        }


    }
}
