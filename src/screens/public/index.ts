import HomePage from './HomePage';
import AuthorDetails from './AuthorDetails';
import BookDetails from './BookDetails';

export default { HomePage, AuthorDetails, BookDetails }