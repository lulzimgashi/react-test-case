// @flow
import * as React from 'react';
import { IReduxStore } from 'src/redux';
import { connect } from 'react-redux';
import { match } from 'react-router';
import { getSingleBookAction, selectBookAction } from 'src/redux/actions/BooksActions/BookActionCreators';
import Loader from 'src/components/common/Loader';
import PhotoList from 'src/components/book-details/PhotoList';
import { useEffect } from 'react';
import { push } from 'connected-react-router';


interface IBookDetailsProps {
    match: match,
    data: any,
    currentBook: any,
    dispatch: (action: any) => void
};



function BookDetails(props: IBookDetailsProps) {

    useEffect(() => {
        return () => {
            props.dispatch(selectBookAction(undefined));
        }
    }, [])


    const goToAuthorDetails = () => {
        props.dispatch(push('/authors/' + props.currentBook.author.id));
    }

    if (!props.currentBook) {
        props.dispatch(getSingleBookAction(props.match.params['bookId']));
        return <div className='loader-wrapper'><Loader /> Loading Book</div>
    }

    return (
        <div className="container">
            <section className="section">
                <h1 className='title'>Book : #{props.currentBook.id}</h1>
            </section>
            <ul>
                <li>ISBN : <strong>{props.currentBook.isbn}</strong></li>
                <li>Title : <strong>{props.currentBook.title}</strong></li>
                <li>Date Published : <strong>{props.currentBook.datePublished}</strong></li>
                <li>Author : <strong><a onClick={goToAuthorDetails}>{props.currentBook.author.name}</a></strong></li>
                <li>Photos : <strong>{props.currentBook.photos.length}</strong></li>
            </ul>


            <h3 style={{ marginBottom: 0, marginTop: '30px' }} className="title is-5">Photos</h3>
            <PhotoList book={props.currentBook} dispatch={props.dispatch} />
        </div>
    );
};


const mapStateToProps = (state: IReduxStore) => {

    return {
        data: state.data,
        currentBook: state.booksOptions.currentSelected
    }
}

export default connect(mapStateToProps)(BookDetails);