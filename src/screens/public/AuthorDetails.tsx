// @flow
import * as React from 'react';
import { connect } from 'react-redux';
import { IReduxStore } from 'src/redux';
import { match } from 'react-router';
import { getSingleAuthorAction, selectAuthorAction } from 'src/redux/actions/AuthorActions/AuthorActionCreators';
import DetailsSection from 'src/components/author-details/DetailsSection';
import Loader from 'src/components/common/Loader';
import BooksTable from 'src/components/author-details/BooksTable';
import PhotoList from 'src/components/author-details/PhotoList';
import { useEffect } from 'react';


interface IAuthorDetailsProps {
    match: match,
    data: any,
    currentAuthor: any,
    dispatch: (action: any) => void
};



function AuthorDetails(props: IAuthorDetailsProps) {

    useEffect(() => {
        return () => {
            props.dispatch(selectAuthorAction(undefined));
        }
    }, [])

    if (!props.currentAuthor) {
        props.dispatch(getSingleAuthorAction(props.match.params['authorId']));
        return <div className='loader-wrapper'><Loader /> Loading Author</div>
    }

    return (
        <div className="container">
            <section className="section">
                <h1 className='title'>Author : {props.currentAuthor.name}</h1>
            </section>

            <DetailsSection currentAuthor={props.currentAuthor} />

            <h3 style={{ marginBottom: 0, marginTop: '30px' }} className="title is-5">Books</h3>
            <BooksTable author={props.currentAuthor} />

            <h3 style={{ marginBottom: 0, marginTop: '30px' }} className="title is-5">Photos</h3>
            <PhotoList author={props.currentAuthor} dispatch={props.dispatch} />
        </div>
    );
};


const mapStateToProps = (state: IReduxStore) => {

    return {
        data: state.data,
        currentAuthor: state.authorsOptions.currentSelected
    }
}

export default connect(mapStateToProps)(AuthorDetails);