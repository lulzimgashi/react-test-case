// @flow
import * as React from 'react';
import Tabs from 'src/components/home-page/Tabs/Tabs';


interface IHomePageProps {
};



export default function (props: IHomePageProps) {


    return (
        <div className="container">
            <section className="section">
                <h1 className='title'>Perdoo Case Study</h1>
            </section>
            <Tabs />
        </div>
    );
};