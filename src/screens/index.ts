import _PrivateRoutes from './private';
import _PublicRoutes from './public';
import PrivateRoute from './common/PrivateRoute';
import PublicRoute from './common/PublicRoute';
import DefaultRoute from './common/DefaultRoute';

export default { _PrivateRoutes, _PublicRoutes, PrivateRoute, PublicRoute, DefaultRoute }


export const DEFAULT_ROUTES_SETTINGS = {
    default_private_route: "/dashboard",
    default_public_route: "/"
}