export default interface CollectionReducerModel {
    currentSelected?: any,
    isLoading: boolean,
    isFetched: boolean,
    currentPage: number,
    nextPagePossible: boolean,
    pagesCount: number,
    pages: PaginationModel
}

interface PaginationModel {
    [key: string]: any
}


export enum CollectionNames {
    AUTHORS_COLLECTION_NAME = "authors",
    BOOKS_COLLECTION_NAME = "books",
    STORES_COLLECTION_NAME = "stores",
    PHOTOS_COLLECTION_NAME = "photos"
}